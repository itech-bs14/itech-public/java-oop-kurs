package _4_Beispiele_Collections.Sets;

import java.util.*;

public class SetInterface_Example {
    Set<String> s1 = new HashSet<String>();  
    Set<String> s2 = new LinkedHashSet<String>();  
    Set<String> s3 = new TreeSet<String>(); 
}
