package _4_Beispiele_Collections.Listen;

import java.util.*;

public class LinkedList_Example {
    public static void main(String args[]) {
        java.util.LinkedList<String> al = new java.util.LinkedList<String>();
        al.add("Ravi");
        al.add("Vijay");
        al.add("Ravi");
        al.add("Ajay");
        Iterator<String> itr = al.iterator();

        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}