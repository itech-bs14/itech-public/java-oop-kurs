package _4_Beispiele_Collections.Listen;

import java.util.*;

public class ListInterface_Example {
    List<String> list1 = new ArrayList<String>();
    List<String> list2 = new LinkedList<String>();
    List<String> list3 = new Vector<String>();
    List<String> list4 = new Stack<String>();
}
