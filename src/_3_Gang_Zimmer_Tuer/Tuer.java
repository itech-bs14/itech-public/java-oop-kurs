package _3_Gang_Zimmer_Tuer;

public class Tuer {
	private Zimmer zi1 = null;
	private Zimmer zi2 = null;
	private Gang ga1 = null;
	private Gang ga2 = null;

	public Tuer() {
	}

	public void setR1(Zimmer z1) {
		this.zi1 = z1;
	}

	public void setR1(Gang g1) {
		this.ga1 = g1;
	}

	public void setR2(Zimmer z2) {
		this.zi2 = z2;
	}

	public void setR2(Gang g2) {
		this.ga2 = g2;
	}

	public boolean istTuerOK() {
		int summe = 0;
		if (zi1 != null)
			summe = summe + 1;
		if (zi2 != null)
			summe = summe + 1;
		if (ga1 != null)
			summe = summe + 1;
		if (ga2 != null)
			summe = summe + 1;

		return (summe == 2) && ((zi1 != zi2) || (ga1 != ga2));
	}
}
