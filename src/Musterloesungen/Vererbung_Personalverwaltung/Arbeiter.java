package Musterloesungen.Vererbung_Personalverwaltung;

public class Arbeiter extends Mitarbeiter {

    private double stdLohn;
    private int stunden;

    public Arbeiter() {
        super();
        this.stdLohn = 0;
        this.stunden = 0;
    }

    public Arbeiter(String name) {
        super(name);
        this.stdLohn = 0;
        this.stunden = 0;
    }

    public void setStunden(int stunden) {
        if (stunden >= 0)
            this.stunden = stunden;
        else
            System.out.println("Falsche eingabe!");
    }

    public void setStdLohn(double stdLohn) {
        if (stdLohn >= 0)
            this.stdLohn = stdLohn;
        else
            System.out.println("Falsche eingabe!");
    }

    public double getBrutto() {
        return stdLohn * stunden;
    }

    @Override
    public String toString() {
        // mit "super" wird die Basisklasse aufgerufen
        String signatur = super.toString() + ", Lohn: " + this.getBrutto() + " €";
        return signatur;
    }
}