package Musterloesungen.Vererbung_Personalverwaltung;

public class Mitarbeiter
{
    private String name;
    private int persNr;
    private static int autoNr=0;
    
    public Mitarbeiter()
    {
        name="unbekannt";
        autoNr++;
        persNr=autoNr;
    }
   
   public Mitarbeiter(String name)
    {
        this();//Parameterloser Konstruktor wird aufgerufen.
        this.name=name;       
    }
    
    public String toString(){
      return "Name: "+ name+", PersNr.: "+persNr;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    
   
}
