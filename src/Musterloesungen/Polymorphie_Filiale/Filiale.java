package Musterloesungen.Polymorphie_Filiale;

public class Filiale {

  private Mitarbeiter[] mitarbeiter = new Mitarbeiter[4];

  public Filiale() {
    // Leiter
    mitarbeiter[3] = new Angestellter("Donald");

    //Mitarbeiter
    mitarbeiter[0] = new Arbeiter("Trick");
    mitarbeiter[1] = new Arbeiter("Tick");
    mitarbeiter[2] = new Arbeiter("Track");

    // Gehalt und Löhne festlegen
    ((Angestellter) mitarbeiter[3]).setBrutto(3500);
    for (int i = 0; i <= 2; i++) {
      ((Arbeiter) mitarbeiter[i]).setStunden(40);
      ((Arbeiter) mitarbeiter[i]).setStdLohn(10.20);
    }
  }

  public void zeigeBeschaeftigte() {

    System.out.println("Alle (!) Mitarbeier: ");
    for (int i = 0; i < mitarbeiter.length; i++) {
      System.out.println(mitarbeiter[i].toString());
    }
  }

  public double zeigePersonalkosten() {
    double summe = 0;
    
    for (Mitarbeiter mit : mitarbeiter) {
      summe += mit.getBrutto();
    }
    
    return summe;
  }

  public static void main(String[] args) {
    Filiale hh = new Filiale();
    hh.zeigeBeschaeftigte();
    System.out.println("Gesamte Personalkosten: " + hh.zeigePersonalkosten() + "€");
  }
}