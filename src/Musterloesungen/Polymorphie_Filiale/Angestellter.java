package Musterloesungen.Polymorphie_Filiale;

public class Angestellter extends Mitarbeiter {

    private double brutto;

    public Angestellter() {
        super();
        this.brutto = 0;
    }

    public Angestellter(String name) {
        super(name);
        this.brutto = 0;
    }

    public void setBrutto(double brutto) {
        if (brutto >= 0)
            this.brutto = brutto;
        else
            System.out.println("Falsche eingabe!");
    }

    public double getBrutto() {
        return brutto;
    }

    public String toString() {
        String signatur = super.toString() + ", Gehalt: " + this.getBrutto() + " €";
        return signatur;
    }
}