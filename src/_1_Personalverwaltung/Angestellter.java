package _1_Personalverwaltung;

public class Angestellter
{
    private String name;
    private double brutto;
    private int persNr;
    private static int autoNr=0;
  
    public Angestellter()
    {
        name="unbekannt";
        autoNr++;
        persNr=autoNr;
        brutto=0;
    }
    
    public Angestellter(String name)
    {
        this();//Konstruktor ohne Parameter wird aufgerufen.
        this.name=name;
    }
    
    public String toString()
    {
      return("Name: "+ name+", PersNr.: "+persNr+", Brutto: "+brutto);
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
      
    public double getBrutto(){
      return brutto;
    }
    
    public void setBrutto(double brutto)
    {
        if(brutto>=0)
            this.brutto=brutto;
        else
            System.out.println("Falsche eingabe!");
    }
}
