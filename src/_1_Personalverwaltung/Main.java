package _1_Personalverwaltung;

public class Main {

  public static void main(String[] args) {
    Angestellter a1 = new Angestellter("Meier");
    System.out.println(a1);
    
    Angestellter a2 = new Angestellter("Schulze");
    System.out.println(a2);
    a2.setBrutto(100);
    System.out.println(a2);
    
    // Konstruktor ohne Parameter:
    Angestellter a3 = new Angestellter();
    System.out.println(a3);
    a3.setName("Schmidt");
    System.out.println(a3);
  }
}
