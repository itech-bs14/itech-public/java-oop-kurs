package _2_Filiale;

public class Filiale {

  private Angestellter leiter;

  private Mitarbeiter[] mitarbeiter = new Arbeiter[3];

  public Filiale() {
    // Leiter
    leiter = new Angestellter("Donald");

    //Mitarbeiter
    mitarbeiter[0] = new Arbeiter("Trick");
    mitarbeiter[1] = new Arbeiter("Tick");
    mitarbeiter[2] = new Arbeiter("Track");

    // Gehalt und Löhne festlegen
    leiter.setBrutto(3500);

    for (int i = 0; i <= 2; i++) {
      ((Arbeiter) mitarbeiter[i]).setStunden(40);
      ((Arbeiter) mitarbeiter[i]).setStdLohn(10.20);
    }
  }

  public void zeigeBeschaeftigte()
  {
      System.out.println("Chef: \n" + leiter.toString());
      
      System.out.println("\n\nMitarbeier: ");
      for(int i=0;i<mitarbeiter.length;i++)
      {
        System.out.println(mitarbeiter[i].toString());
      }
  }

  public static void main(String[] args) {
    Filiale hh = new Filiale();
    hh.zeigeBeschaeftigte();
  }
}